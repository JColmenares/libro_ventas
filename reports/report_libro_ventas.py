from openerp.report import report_sxw
from openerp.osv import osv

class report_nombre(report_sxw.rml_parse):
    def __init__(self,cr,uid,name,context):
        super(report_nombre,self).__init__(cr,uid,name,context)

class report_nombre_des(osv.AbstractModel):
    _name = "report.libro_ventas.libro.libro_ventas"
    _inherit = "report.abstract_report"
    _template = "libro_ventas.template_report_libro_ventas"
    _wrapped_report_class = libro_compra

    
